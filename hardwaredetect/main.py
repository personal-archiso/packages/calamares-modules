#!/usr/bin/env python3

"""
Hardware detection

Fills global storage with 3 keys:
* cpuVendor - e.g. "GenuineIntel", "AuthenticAMD"
* cpuModel - e.g. "Intel(R) Core(TM) i5-9300H CPU @ 2.40GHz"
* gpuDrivers - list of drivers, e.g. ["i915", "nvidia"]
"""

import libcalamares
import subprocess


def pretty_name():
    return "Hardware Detection"


def pretty_status_message():
    return "Detecting Hardware"


def run():
    cpu_vendor = "unknown"
    cpu_model = "unknown"
    gpu_drivers = []

    # 1. Determine CPU Vendor and Model
    try:
        with open("/proc/cpuinfo", "r") as cpu_file:
            for line in cpu_file:
                if line.strip().startswith("vendor_id"):
                    cpu_vendor = line.split(":")[1].strip()
                if line.strip().startswith("model name"):
                    cpu_model = line.split(":")[1].strip()
    except KeyError:
        libcalamares.utils.warning("Failed to get CPU drivers")

    # 2. Determine GPU Drivers
    try:
        lspci_output = subprocess.run("LANG=C lspci -k | grep -EA3 'VGA|3D|Display'",
                                      capture_output=True, shell=True, text=True)

        for line in lspci_output.stdout.split("\n"):
            if line.strip().startswith("Kernel driver in use:"):
                gpu_drivers.append(line.split(":")[1].strip())
    except subprocess.CalledProcessError as cpe:
        libcalamares.utils.warning(f"Failed to get GPU drivers with error: {cpe.output}")
    except KeyError:
        libcalamares.utils.warning("Failed to parse GPU driver string")

    # 3. Insert keys
    libcalamares.globalstorage.insert("cpuVendor", cpu_vendor)
    libcalamares.globalstorage.insert("cpuModel", cpu_model)
    libcalamares.globalstorage.insert("gpuDrivers", gpu_drivers)

    return None
