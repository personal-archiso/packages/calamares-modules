#!/usr/bin/env python3

"""
Hardware package chooser

Based on the output of hardwaredetect determines,
which microcode and which drivers to install.
"""

import libcalamares
import subprocess


def pretty_name():
    return "Hardware Package Chooser"


def pretty_status_message():
    return "Choosing packages for hardware support"


def run():
    cpu_vendor = "unknown"
    gpu_drivers = []
    packages = []
    operations = []

    # 1. Import keys
    if libcalamares.globalstorage.contains("cpuVendor"):
        cpu_vendor = libcalamares.globalstorage.value("cpuVendor")

    if libcalamares.globalstorage.contains("gpuDrivers"):
        gpu_drivers = libcalamares.globalstorage.value("gpuDrivers")

    if libcalamares.globalstorage.contains("packageOperations"):
        operations = libcalamares.globalstorage.value("packageOperations")

    # 2. Determine CPU Microcode
    match cpu_vendor:
        case "GenuineIntel":
            packages += ["intel-ucode"]
        case "AuthenticAMD":
            packages += ["amd-ucode"]

    # 3. Determine GPU Drivers
    for driver in gpu_drivers:
        if driver == "i915":
            packages += ["mesa", "vulkan-intel", "intel-media-driver"]
        if driver == "nvidia":
            packages += ["nvidia", "nvidia-settings"]

    # 4. Add new operation
    if len(packages) != 0:
        entry = {
            "source": "hardwarepackagechooser",
            "install": packages
        }
        # Microcode and drivers should be installed first
        operations = [entry] + operations
        libcalamares.globalstorage.insert("packageOperations", operations)

    return None
